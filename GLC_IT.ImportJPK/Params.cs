﻿using Soneta.Business;
using System;
using GLC_IT.ImportJPK;
using Soneta.Business.UI;
using Soneta.EwidencjaVat;
using Soneta.Types;

namespace GLC_IT.ImportJPK
{
    class Params :ContextBase
    {
        protected string _fullPath = "";
        protected string _dateFormat = "";
        protected bool _debugError = false;
        private Date dataEwidencji;
        private Date dataZaewidencjonowania;

        public Params(Context context) : base(context)
        {
            _fullPath = "";

            systemCheck();
        }

        [Required]
        public string[] FullPath { get; set; }

        public byte[][] FileContent
        {
            get
            {
                IFileSystemService fss;
                this.Session.GetService<IFileSystemService>(out fss);
                byte[][] fileContent = new byte[this.FullPath.Length][];
                int idx = 0;
                if (fss == null)
                    throw new Exception("Nie udało się utworzyć usługi IFileSystemService");

                foreach (string fullP in this.FullPath)
                {
                    fileContent[idx] = fss.ReadFile(fullP);
                    idx++;
                }
                return fileContent;
            }
        }


        [Required]
        public Date DataEwidencji
        {
            get => dataEwidencji;
            set => dataEwidencji = value;
        }


        [AttributeInheritance]
        public Date DataZaewidencjonowania
        {
            get => dataZaewidencjonowania;
            set => dataZaewidencjonowania = value;
        }

        public string DateFormat
        {
            get
            {
                return this._dateFormat;
            }
            set
            {
                this._dateFormat = value;
                this.OnChanged(EventArgs.Empty);
            }
        }


        public bool DebugError
        {
            get
            {
                return this._debugError;
            }

        }

        public object GetListFullPath()
        {
            FileDialogInfo fileDialogInfo = new FileDialogInfo()
            {
                Title = "Wybierz plik",
                Filter = "Wszystkie pliki|*.*"
            };

            return fileDialogInfo;
        }


        internal void check()
        {

            if (string.IsNullOrWhiteSpace(this.FullPath[0]))
                throw new NullReferenceException("Plik do importu");
        }

        internal void systemCheck()
        {
            //throw new NullReferenceException("BŁĄD!");
        }


    }
}
