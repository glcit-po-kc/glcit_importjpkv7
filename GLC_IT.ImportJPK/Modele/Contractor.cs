﻿namespace GLC_IT.ImportJPK.Modele
{
    class Contractor
    {
        string nip;
        string nazwa;
        string adres;

        public string Nip
        {
            get => nip;
            set => nip = value;
        }

        public string Nazwa
        {
            get => nazwa;
            set => nazwa = value;
        }

        public string Adres
        {
            get => adres;
            set => adres = value;
        }
    }
}
