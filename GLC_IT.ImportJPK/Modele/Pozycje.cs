﻿using System;
using System.Collections.Generic;

namespace GLC_IT.ImportJPK.Modele
{
    class Pozycje
    {
        string k_10;
        string k_11;
        string k_12;
        string k_13;
        string k_15;
        string k_16;
        string k_17;
        string k_18;
        string k_19;
        string k_20;
        string k_21;
        string k_22;
        string k_23;
        string k_24;
        string k_25;
        string k_26;
        string k_27;
        string k_28;
        string k_29;
        string k_30;
        string k_31;
        string k_32;
        string k_33;
        string k_34;
        string k_35;
        string k_36;





        public string NetZW
        {
            get => k_10;
            set => k_10 = value;
        }

        public string K_11
        {
            get => k_11;
            set => k_11 = value;
        }

        public string K_12
        {
            get => k_12;
            set => k_12 = value;
        }

        public string Net0
        {
            get => k_13;
            set => k_13 = value;
        }

        public string Net5
        {
            get => k_15;
            set => k_15 = value;
        }

        public string Vat5
        {
            get => k_16;
            set => k_16 = value;
        }

        public string Net8
        {
            get => k_17;
            set => k_17 = value;
        }

        public string Vat8
        {
            get => k_18;
            set => k_18 = value;
        }

        public string Net23
        {
            get => k_19;
            set => k_19 = value;
        }

        public string Vat23
        {
            get => k_20;
            set => k_20 = value;
        }

        public string K_21
        {
            get => k_21;
            set => k_21 = value;
        }

        public string K_22
        {
            get => k_22;
            set => k_22 = value;
        }


        public string K_23
        {
            get => k_23;
            set => k_23 = value;
        }

        public string K_24
        {
            get => k_24;
            set => k_24 = value;
        }

        public string K_25
        {
            get => k_25;
            set => k_25 = value;
        }

        public string K_26
        {
            get => k_26;
            set => k_26 = value;
        }

        public string K_27
        {
            get => k_27;
            set => k_27 = value;
        }

        public string K_28
        {
            get => k_28;
            set => k_28 = value;
        }

        public string K_29
        {
            get => k_29;
            set => k_29 = value;
        }

        public string K_30
        {
            get => k_30;
            set => k_30 = value;
        }

        public string K_31
        {
            get => k_31;
            set => k_31 = value;
        }

        public string K_32
        {
            get => k_32;
            set => k_32 = value;
        }

        public string K_33
        {
            get => k_33;
            set => k_33 = value;
        }

        public string K_34
        {
            get => k_34;
            set => k_34 = value;
        }

        public string K_35
        {
            get => k_35;
            set => k_35 = value;
        }
        public string K_36
        {
            get => k_36;
            set => k_36 = value;
        }



        public int Count
        {
            get
            {
                int count = 0;
                if (k_10 != null) count++;
                if (k_11 != null) count++;
                if (k_12 != null) count++;
                if (k_13 != null) count++;
                if (k_15 != null) count++;
                if (k_17 != null) count++;
                if (k_19 != null) count++;
                if (k_21 != null) count++;
                if (k_22 != null) count++;

                return count;
            }
        }

        public List<string[]> Lista
        {
            get
            {
                List<string[]> poz = new List<string[]>();

                if ((k_10 != null) && (k_10 != "0.00" && k_10 != "0"))
                    poz.Add(new string[] { "ZW", k_10, "0.00", "K_10", "krajowy" });

                if ((k_11 != null) && (k_11 != "0.00" && k_11 != "0"))
                    poz.Add(new string[] { "NP", k_11, "0.00", "K_11", "unijny" });

                if ((k_12 != null) && (k_12 != "0.00" && k_12 != "0"))
                    poz.Add(new string[] { "NP", k_12, "0.00", "K_12", "unijny" });

                if ((k_13 != null) && (k_13 != "0.00" && k_13 != "0"))
                    poz.Add(new string[] { "0%", k_13, "0,00", "K_13", "krajowy" });

                if ((k_15 != null) && (k_15 != "0.00" && k_15 != "0"))
                    poz.Add(new string[] { "5%", k_15, k_16, "K_15", "krajowy" });

                if ((k_17 != null) && (k_17 != "0.00" && k_17 != "0"))
                    poz.Add(new string[] { "8%", k_17, k_18, "K_17", "krajowy" });

                if ((k_19 != null) && (k_19 != "0.00" && k_19 != "0"))
                    poz.Add(new string[] { "23%", k_19, k_20, "K_19", "krajowy" });

                if ((k_21 != null) && (k_21 != "0.00" && k_21 != "0"))
                    poz.Add(new string[] { "0%", k_21, "0,00", "K_21", "unijny" });

                if ((k_22 != null) && (k_22 != "0.00" && k_22 != "0"))
                    poz.Add(new string[] { "0%", k_22, "0,00", "K_22", "eksportowy" });

                if ((k_23 != null) && (k_23 != "0.00" && k_23 != "0"))
                    poz.Add(new string[] { Funkcje.ProcentStawki(Convert.ToDecimal(k_23.Replace('.', ',')), Convert.ToDecimal(k_24.Replace('.', ','))), k_23, k_24, "K_23", "unijny" });

                if ((k_27 != null) && (k_27 != "0.00" && k_27 != "0"))
                    poz.Add(new string[] { Funkcje.ProcentStawki(Convert.ToDecimal(k_27.Replace('.', ',')), Convert.ToDecimal(k_28.Replace('.', ','))), k_27, k_28, "K_27", "eksportowy" });

                if ((k_29 != null) && (k_29 != "0.00" && k_29 != "0"))
                    poz.Add(new string[] { Funkcje.ProcentStawki(Convert.ToDecimal(k_29.Replace('.', ',')), Convert.ToDecimal(k_30.Replace('.', ','))), k_29, k_30, "K_29", "unijny" });

                if ((k_31 != null) && (k_31 != "0.00" && k_31 != "0"))
                    poz.Add(new string[] { "NP", k_31, "0.00", "K_31", "krajowy" });


                if ((k_32 != null) && (k_32 != "0.00" && k_32 != "0"))
                    poz.Add(new string[] { Funkcje.ProcentStawki(Convert.ToDecimal(k_32.Replace('.', ',')), Convert.ToDecimal(k_33.Replace('.', ','))), k_32, k_33, "K_32", "krajowy" });

                if ((k_34 != null) && (k_34 != "0.00" && k_34 != "0"))
                    poz.Add(new string[] { Funkcje.ProcentStawki(Convert.ToDecimal(k_34.Replace('.', ',')), Convert.ToDecimal(k_35.Replace('.', ','))), k_34, k_35, "K_34", "krajowy" });
                /*
                if ((k_43 != null) && (k_43 != "0.00" && k_43 != "0"))
                    poz.Add(new string[] { Funkcje.ProcentStawki(Convert.ToDecimal(k_43.Replace('.', ',')), Convert.ToDecimal(k_44.Replace('.', ','))), k_43, k_44, "K_43", "krajowy" });

                if ((k_44 != null) && ((k_43 == "0.00" || k_43 == "0") && (k_44 != "0.00" && k_44 != "0")))
                    poz.Add(new string[] { "23%", "0.00", k_44, "K_44", "krajowy" });

                if ((k_45 != null) && (k_45 != "0.00" && k_45 != "0"))
                    poz.Add(new string[] { Funkcje.ProcentStawki(Convert.ToDecimal(k_45.Replace('.', ',')), Convert.ToDecimal(k_46.Replace('.', ','))), k_45, k_46, "K_45", "krajowy" });

                if ((k_46 != null) && ((k_45 == "0.00" || k_45 == "0") && (k_46 != "0.00" && k_46 != "0")))
                    poz.Add(new string[] { "23%", "0.00", k_46, "K_46", "krajowy" });

                if ((k_49 != null) && (k_49 != "0.00" && k_49 != "0"))
                    poz.Add(new string[] { "23%", "0.00", k_49, "K_49", "" });

                if ((k_50 != null) && (k_50 != "0.00" && k_50 != "0"))
                    poz.Add(new string[] { "23%", "0.00", k_50, "K_50", "" });
                */

                return poz;
            }
        }


    }
}
