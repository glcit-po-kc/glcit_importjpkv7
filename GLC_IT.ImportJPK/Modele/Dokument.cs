﻿using System;
using Soneta.Types;

namespace GLC_IT.ImportJPK.Modele
{
    class Dokument
    {

        string currentSymbol;
        Contractor contractor;
        string numerDokumentu;
        DateTime dataWystawienia;
        DateTime dataSprzedazy;
        DateTime dataWplywu;
        Pozycje pozycje;

       


        string NP;
        // pozycje deklaracji

        decimal _P_10;
        decimal _P_11;
        decimal _P_12;
        decimal _P_13;
        decimal _P_14;
        decimal _P_15;
        decimal _P_16;
        decimal _P_17;
        decimal _P_18;
        decimal _P_19;
        decimal _P_20;
        decimal _P_21;
        decimal _P_22;
        decimal _P_23;
        decimal _P_24;
        decimal _P_25;
        decimal _P_26;
        decimal _P_27;
        decimal _P_28;
        decimal _P_29;
        decimal _P_30;
        decimal _P_31;
        decimal _P_32;
        decimal _P_33;
        decimal _P_34;
        decimal _P_35;
        decimal _P_36;
        decimal _P_37;
        decimal _P_38;
        decimal _P_39;
        decimal _P_40;
        decimal _P_41;
        decimal _P_42;
        decimal _P_43;
        decimal _P_44;
        decimal _P_45;
        decimal _P_46;
        decimal _P_47;
        decimal _P_48;
        decimal _P_49;
        decimal _P_50;
        decimal _P_51;
        decimal _P_52;
        decimal _P_53;
        decimal _P_54;
        decimal _P_55;
        decimal _P_56;
        decimal _P_57;
        decimal _P_58;
        decimal _P_59;
        decimal _P_60;
        decimal _P_61;
        string _P_62;
        decimal _P_63;
        decimal _P_64;
        decimal _P_65;
        decimal _P_66;
        decimal _P_67;
        decimal _P_68;
        decimal _P_69;

        string _P_ORDZU;

        string pouczenia;
        string _P_3A;
        string _P_3B;
        string _P_3C;
        string _P_3D;
        string _P_4B;
        string _P_5B;
        Date _P_6;
        //Procedury Vat


        // Kody GTU
        string GTU1 = "01";
        string GTU2 = "02";
        string GTU3 = "03";
        string GTU4 = "04";
        string GTU5 = "05";
        string GTU6 = "06";
        string GTU7 = "07";
        string GTU8 = "08";
        string GTU9 = "09";
        string GTU10 = "10";
        string GTU11 = "11";
        string GTU12 = "12";
        string GTU13 = "13";

        public string GTU_01 { set => GTU1 = value; }
        public string GTU_02 { set => GTU2 = value; }
        public string GTU_03 { set => GTU3 = value; }
        public string GTU_04 { set => GTU4 = value; }
        public string GTU_05 { set => GTU5 = value; }
        public string GTU_06 { set => GTU6 = value; }
        public string GTU_07 { set => GTU7 = value; }
        public string GTU_08 { set => GTU8 = value; }
        public string GTU_09 { set => GTU9 = value; }
        public string GTU_10 { set => GTU10 = value; }
        public string GTU_11 { set => GTU11 = value; }
        public string GTU_12 { set => GTU12 = value; }
        public string GTU_13 { set => GTU13 = value; }

        //procedury VAT
        string procedury;

        public string ProceduryVat { get => procedury; set => procedury = value; }

        public string NIPPodmiotu { get => NP; set => NP = value; }



        public  string Pouczenia { get => pouczenia; set => pouczenia = value; }
        public string AdresNabywcy { get => _P_3B; set => _P_3B = value; }
        public string NazwaSprzedawcy { get => _P_3C; set => _P_3C = value; }
        public string AdresSprzedawcy { get => _P_3D; set => _P_3D = value; }
        public string NIPSprzedawcy { get => _P_4B; set => _P_4B = value; }
        public string NIPNabywcy { get => _P_5B; set => _P_5B = value; }

        public Date DataOperacji { get => _P_6; set => _P_6 = value; }

        // pozycje deklaracji
        public dynamic P_10 { get => _P_10; set => _P_10 = value; }
        public dynamic P_11 { get => _P_11; set => _P_11 = value; }
        public dynamic P_12 { get => _P_12; set => _P_12 = value; }
        public dynamic P_13 { get => _P_13; set => _P_13 = value; }
        public dynamic P_14 { get => _P_14; set => _P_14 = value; }
        public dynamic P_15 { get => _P_15; set => _P_15 = value; }
        public dynamic P_16 { get => _P_16; set => _P_16 = value; }
        public dynamic P_17 { get => _P_17; set => _P_17 = value; }
        public dynamic P_18 { get => _P_18; set => _P_18 = value; }
        public dynamic P_19 { get => _P_19; set => _P_19 = value; }
        public dynamic P_20 { get => _P_20; set => _P_20 = value; }
        public dynamic P_21 { get => _P_21; set => _P_21 = value; }
        public dynamic P_22 { get => _P_22; set => _P_22 = value; }
        public dynamic P_23 { get => _P_23; set => _P_23 = value; }
        public dynamic P_24 { get => _P_24; set => _P_24 = value; }
        public dynamic P_25 { get => _P_25; set => _P_25 = value; }
        public dynamic P_26 { get => _P_26; set => _P_26 = value; }
        public dynamic P_27 { get => _P_27; set => _P_27 = value; }
        public dynamic P_28 { get => _P_28; set => _P_28 = value; }
        public dynamic P_29 { get => _P_29; set => _P_29 = value; }
        public dynamic P_30 { get => _P_30; set => _P_30 = value; }
        public dynamic P_31 { get => _P_31; set => _P_31 = value; }
        public dynamic P_32 { get => _P_32; set => _P_32 = value; }
        public dynamic P_33 { get => _P_33; set => _P_33 = value; }
        public dynamic P_34 { get => _P_34; set => _P_34 = value; }
        public dynamic P_35 { get => _P_35; set => _P_35 = value; }
        public dynamic P_36 { get => _P_36; set => _P_36 = value; }
        public dynamic P_37 { get => _P_37; set => _P_37 = value; }
        public dynamic P_38 { get => _P_38; set => _P_38 = value; }
        public dynamic P_39 { get => _P_39; set => _P_39 = value; }
        public dynamic P_40 { get => _P_40; set => _P_40 = value; }
        public dynamic P_41 { get => _P_41; set => _P_41 = value; }
        public dynamic P_42 { get => _P_42; set => _P_42 = value; }
        public dynamic P_43 { get => _P_43; set => _P_43 = value; }
        public dynamic P_44 { get => _P_44; set => _P_44 = value; }
        public dynamic P_45 { get => _P_45; set => _P_45 = value; }
        public dynamic P_46 { get => _P_46; set => _P_46 = value; }
        public dynamic P_47 { get => _P_47; set => _P_47 = value; }
        public dynamic P_48 { get => _P_48; set => _P_48 = value; }
        public dynamic P_49 { get => _P_49; set => _P_49 = value; }
        public dynamic P_50 { get => _P_50; set => _P_50 = value; }
        public dynamic P_51 { get => _P_51; set => _P_51 = value; }
        public dynamic P_52 { get => _P_52; set => _P_52 = value; }
        public dynamic P_53 { get => _P_53; set => _P_53 = value; }
        public dynamic P_54 { get => _P_54; set => _P_54 = value; }
        public dynamic P_55 { get => _P_55; set => _P_55 = value; }
        public dynamic P_56 { get => _P_56; set => _P_56 = value; }
        public dynamic P_57 { get => _P_57; set => _P_57 = value; }
        public dynamic P_58 { get => _P_58; set => _P_58 = value; }
        public dynamic P_59 { get => _P_59; set => _P_59 = value; }
        public dynamic P_60 { get => _P_60; set => _P_60 = value; }
        public dynamic P_61 { get => _P_61; set => _P_61 = value; }
        public dynamic P_62 { get => _P_62; set => _P_62 = value; }
        public dynamic P_63 { get => _P_63; set => _P_63 = value; }
        public dynamic P_64 { get => _P_64; set => _P_64 = value; }
        public dynamic P_65 { get => _P_65; set => _P_65 = value; }
        public dynamic P_66 { get => _P_66; set => _P_66 = value; }
        public dynamic P_67 { get => _P_67; set => _P_67 = value; }
        public dynamic P_68 { get => _P_68; set => _P_68 = value; }
        public dynamic P_69 { get => _P_69; set => _P_69 = value; }
        public string P_ORDZU { get => _P_ORDZU; set => _P_ORDZU = value; }




        public Dokument()
        {
            contractor = new Contractor();
            pozycje = new Pozycje();
        }


        public Contractor Contractor
        {
            get => contractor;
            set => contractor = value;
        }

        public string NumerDokumentu
        {
            get => numerDokumentu;
            set => numerDokumentu = value;
        }

        public DateTime DataWystawienia
        {
            get => dataWystawienia;
            set => dataWystawienia = value;
        }

        public DateTime DataSprzedazy
        {
            get => dataSprzedazy;
            set => dataSprzedazy = value;
        }

        public DateTime DataWplywu
        {
            get => dataWplywu;
            set => dataWplywu = value;
        }

        public Pozycje Pozycje
        {
            get => pozycje;
            set => pozycje = value;
        }

        public string TypDefinicji
        {
            get
            {
                string symbol = "";
                foreach (string[] s in this.Pozycje.Lista)
                {
                    symbol = s[3];
                    break;
                }

                switch(symbol)
                {
                    case "K_10":
                    case "K_11":
                    case "K_12":
                    case "K_15":
                    case "K_17":
                    case "K_19":
                    case "K_21":
                    case "K_22":
                    case "K_31":
                        return "Sprzedaz";
                    case "K_23":
                    case "K_27":
                    case "K_29":
                    case "K_32":
                    case "K_34":
                        return "Nabycia nalezny";
                    case "K_43":
                    case "K_45":
                        return "Zakup";
                    case "K_44":
                    case "K_46":
                        return "SAD";
                    case "K_49":
                    case "K_50":
                        return "RKV";
                    default:
                        return "";

                }
            }
        }


    }
}
