﻿using System;
using Soneta.Business;
using Soneta.Config;
using Soneta.CRM;
using Soneta.Import;
using Soneta.EwidencjaVat;
using GLC_IT.ImportJPK;

[assembly: Worker(typeof(KonfiguracjaJPK))]

namespace GLC_IT.ImportJPK
{
    class KonfiguracjaJPK:ISessionable
    {
        public KonfiguracjaJPK()
        {
        }

        [Context]
        public Session Session { get; set; }

        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_10
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_10", "")] ; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_10", value.Symbol, AttributeType._string); }
        }
        
        public RodzajSprzedazyVAT K_10_R
        {
            get
            {
                string typ = "Towar";
                if(GetValue("K_10_R", "") != "")
                    typ = GetValue("K_10_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_10_R", value.ToString(), AttributeType._string); }
        }

        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_11
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_11", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_11", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_11_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_11_R", "") != "")
                    typ = GetValue("K_11_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_11_R", value.ToString(), AttributeType._string); }
        }


        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_12
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_12", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_12", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_12_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_12_R", "") != "")
                    typ = GetValue("K_12_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_12_R", value.ToString(), AttributeType._string); }
        }

        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_15
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_15", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_15", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_15_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_15_R", "") != "")
                    typ = GetValue("K_15_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_15_R", value.ToString(), AttributeType._string); }
        }

        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_17
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_17", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_17", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_17_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_17_R", "") != "")
                    typ = GetValue("K_17_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set {SetValue("K_17_R", value.ToString(), AttributeType._string); }
        }

        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_19
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_19", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_19", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_19_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_19_R", "") != "")
                    typ = GetValue("K_19_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_19_R", value.ToString(), AttributeType._string); }
        }

        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_21
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_21", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_21", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_21_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_21_R", "") != "")
                    typ = GetValue("K_21_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_21_R", value.ToString(), AttributeType._string); }
        }

        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_22
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_22", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_22", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_22_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_22_R", "") != "")
                    typ = GetValue("K_22_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_22_R", value.ToString(), AttributeType._string); }
        }

        public FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok K_23
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_23", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_23", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_23_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_23_R", "") != "")
                    typ = GetValue("K_23_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_23_R", value.ToString(), AttributeType._string); }
        }

        public FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok K_27
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_27", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_27", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_27_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_27_R", "") != "")
                    typ = GetValue("K_27_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_27_R", value.ToString(), AttributeType._string); }
        }

        public FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok K_29
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_29", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_29", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_29_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_29_R", "") != "")
                    typ = GetValue("K_29_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_29_R", value.ToString(), AttributeType._string); }
        }

        public SprzedazEwidencja.SprzedazEwidencjaDefDok K_31
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SprzedazEwidencja.SprzedazEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_31", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_31", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_31_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_31_R", "") != "")
                    typ = GetValue("K_31_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_31_R", value.ToString(), AttributeType._string); }
        }

        public FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok K_32
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_32", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_32", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_32_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_32_R", "") != "")
                    typ = GetValue("K_32_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_32_R", value.ToString(), AttributeType._string); }
        }



        public FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok K_34
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (FWUENabyciaNależnyEwidencja.FWUENabyciaNależnyEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_34", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_34", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_34_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_34_R", "") != "")
                    typ = GetValue("K_34_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_34_R", value.ToString(), AttributeType._string); }
        }

        public ZakupEwidencja.ZakupEwidencjaDefDok K_43
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (ZakupEwidencja.ZakupEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_43", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_43", value.Symbol, AttributeType._string); }
        }

        public RodzajZakupuVAT K_43_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_43_R", "") != "")
                    typ = GetValue("K_43_R", "");
                return (RodzajZakupuVAT)Enum.Parse(typeof(RodzajZakupuVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_43_R", value.ToString(), AttributeType._string); }
        }


        public SADEwidencja.SADEwidencjaDefDok K_44
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SADEwidencja.SADEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_44", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_44", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_44_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_44_R", "") != "")
                    typ = GetValue("K_44_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_44_R", value.ToString(), AttributeType._string); }
        }




        public ZakupEwidencja.ZakupEwidencjaDefDok K_45
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (ZakupEwidencja.ZakupEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_45", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_45", value.Symbol, AttributeType._string); }
        }

        public RodzajZakupuVAT K_45_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_45_R", "") != "")
                    typ = GetValue("K_45_R", "");
                return (RodzajZakupuVAT)Enum.Parse(typeof(RodzajZakupuVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_45_R", value.ToString(), AttributeType._string); }
        }


        public SADEwidencja.SADEwidencjaDefDok K_46
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (SADEwidencja.SADEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_46", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_46", value.Symbol, AttributeType._string); }
        }

        public RodzajSprzedazyVAT K_46_R
        {
            get
            {
                string typ = "Towar";
                if (GetValue("K_46_R", "") != "")
                    typ = GetValue("K_46_R", "");
                return (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), typ);
            }

            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_46_R", value.ToString(), AttributeType._string); }
        }


        public RozliczeniaKasoweVATEwidencja.RozliczeniaKasoweVATEwidencjaDefDok K_49
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (RozliczeniaKasoweVATEwidencja.RozliczeniaKasoweVATEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_49", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_49", value.Symbol, AttributeType._string); }
        }

        public RozliczeniaKasoweVATEwidencja.RozliczeniaKasoweVATEwidencjaDefDok K_50
        {
            get { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); return (RozliczeniaKasoweVATEwidencja.RozliczeniaKasoweVATEwidencjaDefDok)ev.Core.DefDokumentow.WgSymbolu[GetValue("K_50", "")]; }
            set { EwidencjaVatModule ev = EwidencjaVatModule.GetInstance(this.Session); SetValue("K_50", value.Symbol, AttributeType._string); }
        }


        //Metoda odpowiada za ustawianie wartosci parametrów konfiguracji
        private void SetValue<T>(string name, T value, AttributeType type)
        {
            SetValue(Session, name, value, type);
        }

        //Metoda odpowiada za pobieranie wartosci parametrów konfiguracji
        private T GetValue<T>(string name, T def)
        {
            return GetValue(Session, name, def);
        }

        //Metoda odpowiada za ustawianie wartosci parametrów konfiguracji
        public static void SetValue<T>(Session session, string name, T value, AttributeType type)
        {
            using (var t = session.Logout(true))
            {
                var cfgManager = new CfgManager(session);
                //wyszukiwanie gałęzi głównej 
                var node1 = cfgManager.Root.FindSubNode("importJPK", false) ??
                            cfgManager.Root.AddNode("importJPK", CfgNodeType.Node);

                //wyszukiwanie liścia 
                var node2 = node1.FindSubNode("KonfiguracjaJPK", false) ??
                            node1.AddNode("KonfiguracjaJPK", CfgNodeType.Leaf);

                //wyszukiwanie wartosci atrybutu w liściu 
                var attr = node2.FindAttribute(name, false);
                if (attr == null)
                    node2.AddAttribute(name, type, value.ToString());
                else
                    attr.Value = value.ToString();

                t.CommitUI();
            }
        }

        //Metoda odpowiada za pobieranie wartosci parametrów konfiguracji
        public static T GetValue<T>(Session session, string name, T def)
        {
            var cfgManager = new CfgManager(session);

            var node1 = cfgManager.Root.FindSubNode("importJPK", false);

            //Jeśli nie znaleziono gałęzi, zwracamy wartosć domyślną
            if (node1 == null) return def;

            var node2 = node1.FindSubNode("KonfiguracjaJPK", false);
            if (node2 == null) return def;

            var attr = node2.FindAttribute(name, false);
            if (attr == null) return def;

            if (attr.Value == null) return def;

            return (T)attr.Value;
        }

    }
}
