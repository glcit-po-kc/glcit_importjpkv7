﻿using System;
using System.Collections.Generic;
using System.Xml;
using Soneta.Business;
using Soneta.Core;
using GLC_IT.ImportJPK;
using GLC_IT.ImportJPK.Modele;
using System.Xml.XPath;
using Soneta.Core.Extensions;
using Soneta.Types.Extensions;
using Soneta.Kasa;

[assembly: Worker(typeof(ImportWorker), typeof(DokEwidencja))]

namespace GLC_IT.ImportJPK
{
    class ImportWorker : ContextBase
    {

        public ImportWorker(Context context) : base(context)
        {

        }

        public ImportWorker(Params Param) : base((Context)null)
        {
            this.Params = Param;
        }


        protected Params _params;

        [Context]
        public Params Params
        {
            get => _params;
            set => _params = value;
        }


        [Action("Import faktur z JPKV7", Mode = ActionMode.SingleSession | ActionMode.OnlyTable | ActionMode.Progress)]
        public object Import()
        {
            int count = 0;

            if (Params.FullPath == null)
                throw new Exception("Nie wybrano pliku!");

            XmlTextReader Reader = new XmlTextReader(Params.FullPath[0]);
            Reader.WhitespaceHandling = WhitespaceHandling.None;
            Reader.Namespaces = true;
            XmlDocument doc = new XmlDocument();

            XPathDocument x = new XPathDocument(Params.FullPath[0]);
            XPathNavigator foo = x.CreateNavigator();
            foo.MoveToFollowing(XPathNodeType.Element);

            string prefix = string.Empty;
            if (foo.Prefix != string.Empty)
                prefix = foo.Prefix + ":";

            IDictionary<string, string> namespaces = foo.GetNamespacesInScope(XmlNamespaceScope.All);

            doc.Load(Reader);

            XmlNamespaceManager manager = new XmlNamespaceManager(doc.NameTable);
            foreach (var ns in namespaces)
            {
                manager.AddNamespace(ns.Key, ns.Value);
            }

            string rootName = string.Empty;
            List<Dokument> listaSprzedazy = new List<Dokument>();
            List<Dokument> Deklaracja = new List<Dokument>();

            List<Dokument> listaZakupu = new List<Dokument>();

            //zmiany JPK V7

            Dictionary<string, string> ZnacznikP = new Dictionary<string, string>();

            XmlElement root1 = doc.DocumentElement;
            string currentSymbol = "";

            string NIPPodmiotu = string.Empty;

            foreach (XmlNode node in root1.ChildNodes)
            {
                if (node.Name.Split(':')[1] == "Podmiot1")
                {
                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (child.Name.Split(':')[1] == "OsobaNiefizyczna")
                        {
                            foreach (XmlNode child2 in child.ChildNodes)
                            {
                                if (child2.Name.Split(':')[1] == "NIP")
                                    NIPPodmiotu = child2.InnerText;
                            }
                        }
                        if (child.Name.Split(':')[1] == "OsobaFizyczna")
                        {
                            foreach (XmlNode child2 in child.ChildNodes)
                            {
                                if (child2.Name.Split(':')[1] == "NIP")
                                    NIPPodmiotu = child2.InnerText;
                            }
                        }
                        if (NIPPodmiotu == string.Empty)
                            throw new Exception("Błąd odczytu Identyfikatora podmiotu!");
                    }
                }

                if (node.Name.Split(':')[1] == "Deklaracja")
                {
                    Dokument Dokument = new Dokument();
                    Dokument.NIPPodmiotu = NIPPodmiotu;

                    List<Dokument> dd = new List<Dokument>();
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name.Split(':')[1] == "PozycjeSzczegolowe")
                        {
                            foreach (XmlNode childNode2 in childNode)
                            {
                                if (childNode2.InnerText != String.Empty)
                                {

                                    currentSymbol = childNode2.Name.Split(':')[1];

                                    if (currentSymbol == "P_10")
                                    {
                                        ZnacznikP.Add(childNode2.Name, childNode2.InnerXml);
                                        Dokument znacznik = new Dokument();// = Convert.ToDecimal(childNode2.InnerText);
                                        Dokument.P_10 = Convert.ToDecimal(childNode2.InnerText);

                                        Log log = new Log();
                                        log.WriteLine(ZnacznikP.Keys.ToString());
                                    }
                                    if (currentSymbol == "P_11")
                                    {
                                        Dokument.P_11 = Convert.ToDecimal(childNode2.InnerText);
                                    }
                                    if (currentSymbol == "P_12")
                                    {
                                        Dokument.P_12 = Convert.ToDecimal(childNode2.InnerText);
                                    }
                                    if (currentSymbol == "P_13")
                                    {
                                        Dokument.P_13 = Convert.ToDecimal(childNode2.InnerText);
                                    }
                                    if (currentSymbol == "P_14")
                                    {
                                        Dokument.P_14 = Convert.ToDecimal(childNode2.InnerText);
                                    }
                                    if (currentSymbol == "P_15")
                                    {
                                        Dokument.P_15 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_16")
                                    {
                                        Dokument.P_16 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_17")
                                    {
                                        Dokument.P_17 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_18")
                                    {
                                        Dokument.P_18 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_19")
                                    {
                                        Dokument.P_19 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_20")
                                    {
                                        Dokument.P_19 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_21")
                                    {
                                        Dokument.P_19 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_22")
                                    {
                                        Dokument.P_19 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_23")
                                    {
                                        Dokument.P_19 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_24")
                                    {
                                        Dokument.P_19 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_125")
                                    {
                                        Dokument.P_19 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_26")
                                    {
                                        Dokument.P_19 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_27")
                                    {
                                        Dokument.P_27 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_28")
                                    {
                                        Dokument.P_28 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_29")
                                    {
                                        Dokument.P_29 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_30")
                                    {
                                        Dokument.P_30 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_31")
                                    {
                                        Dokument.P_31 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_32")
                                    {
                                        Dokument.P_33 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_34")
                                    {
                                        Dokument.P_34 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_35")
                                    {
                                        Dokument.P_35 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_36")
                                    {
                                        Dokument.P_36 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_37")
                                    {
                                        Dokument.P_37 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_38")
                                    {
                                        Dokument.P_38 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_39")
                                    {
                                        Dokument.P_39 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_40")
                                    {
                                        Dokument.P_40 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_41")
                                    {
                                        Dokument.P_41 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_42")
                                    {
                                        Dokument.P_42 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_43")
                                    {
                                        Dokument.P_43 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_44")
                                    {
                                        Dokument.P_44 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_45")
                                    {
                                        Dokument.P_45 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_46")
                                    {
                                        Dokument.P_46 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_47")
                                    {
                                        Dokument.P_47 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_48")
                                    {
                                        Dokument.P_48 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_49")
                                    {
                                        Dokument.P_49 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_50")
                                    {
                                        Dokument.P_50 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_51")
                                    {
                                        Dokument.P_51 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_52")
                                    {
                                        Dokument.P_52 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_53")
                                    {
                                        Dokument.P_53 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_54")
                                    {
                                        Dokument.P_54 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_55")
                                    {
                                        Dokument.P_55 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_56")
                                    {
                                        Dokument.P_56 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_57")
                                    {
                                        Dokument.P_57 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_58")
                                    {
                                        Dokument.P_58 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_59")
                                    {
                                        Dokument.P_59 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_60")
                                    {
                                        Dokument.P_60 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_61")
                                    {
                                        Dokument.P_61 = (Convert.ToDecimal(childNode2.InnerText));
                                    }
                                    if (currentSymbol == "P_62")
                                    {
                                        Dokument.P_62 = childNode2.InnerText;
                                    }
                                    if (currentSymbol == "P_63")
                                    {
                                        Dokument.P_63 = childNode2.InnerText;
                                    }
                                    if (currentSymbol == "P_64")
                                    {
                                        Dokument.P_64 = childNode2.InnerText;
                                    }
                                    if (currentSymbol == "P_65")
                                    {
                                        Dokument.P_65 = childNode2.InnerText;
                                    }
                                    if (currentSymbol == "P_66")
                                    {
                                        Dokument.P_66 = childNode2.InnerText;
                                    }
                                    if (currentSymbol == "P_67")
                                    {
                                        Dokument.P_67 = childNode2.InnerText;
                                    }
                                    if (currentSymbol == "P_68")
                                    {
                                        Dokument.P_68 = childNode2.InnerText;
                                    }
                                    if (currentSymbol == "P_69")
                                    {
                                        Dokument.P_69 = childNode2.InnerText;
                                    }
                                    if (currentSymbol == "P_ORDZU")
                                    {
                                        Dokument.P_ORDZU = childNode2.InnerText;
                                    }
                                    
                                    /*
                                    if (childNode2.InnerText == "1")
                                    {
                                        ZnacznikP.Add(childNode2.Name, childNode2.InnerXml);
                                        Dokument znacznik = new Dokument();// = Convert.ToDecimal(childNode2.InnerText);
                                                                           //Dokument.P_11 + currentSymbol.ToString() = childNode2.InnerText;
                                        Log log = new Log();
                                        log.WriteLine(ZnacznikP.Keys.ToString());
                                    }
                                    */
                                }
                                
                            }
                        }
                        if (childNode.Name.Split(':')[1] == "Pouczenia")
                        {
                            Dokument.Pouczenia = childNode.InnerText;
                        }
                    }
                    Deklaracja.Add(Dokument);
                }
                if (node.Name.Split(':')[1] == "Ewidencja")
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name.Split(':')[1] == "SprzedazWiersz")
                        {
                            XmlNodeList root = doc.SelectNodes(@"//" + prefix + "SprzedazWiersz", manager);
                            rootName = root.Item(0).Name;
                            listaSprzedazy = Funkcje.GetDocumentsList(root, prefix, manager);
                            count += Funkcje.ImportDocuments(listaSprzedazy, this.Session, rootName, Params);
                        }
                        if (childNode.Name.Split(':')[1] == "ZakupWiersz")
                        {
                            XmlNodeList root = doc.SelectNodes(@"//" + prefix + "ZakupWiersz", manager);
                            rootName = root.Item(0).Name;
                            listaSprzedazy = Funkcje.GetDocumentsList(root, prefix, manager);
                            count += Funkcje.ImportDocuments(listaSprzedazy, this.Session, rootName, Params);
                        }
                    }
                }
            }
            //

            /*
            if (root.Count > 0)
            {
                
            }
            */
            /*
            root = doc.SelectNodes(@"//" + prefix + "ZakupWiersz", manager);

            if (root.Count > 0)
            {
                rootName = root.Item(0).Name;
                listaZakupu = Funkcje.GetDocumentsList(root, prefix, manager);
                count += Funkcje.ImportDocuments(listaZakupu, this.Session, rootName, Params);
            }
            */
            return "Zaimportowano " + count.ToString() + " dokumentów.";
        }
    }
}

