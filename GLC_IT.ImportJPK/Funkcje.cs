﻿using System;
using System.Collections.Generic;
using System.Linq;
using Soneta.Import;
using System.Xml;
using Soneta.Business;
using Soneta.Core;
using Soneta.CRM;
using Soneta.EwidencjaVat;
using Soneta.Kasa;
using Soneta.Ksiega;
using GLC_IT.ImportJPK.Modele;

namespace GLC_IT.ImportJPK
{
    class Funkcje
    {
        public static List<Dokument> GetDocumentsList(XmlNodeList root, string prefix, XmlNamespaceManager manager)
        {
            List<Dokument> lista = new List<Dokument>();

            foreach (XmlNode child in root)
            {
                Dokument dokument = new Dokument();

                if (child.SelectSingleNode(prefix + "NrKontrahenta", manager) != null)
                    dokument.Contractor.Nip = child.SelectSingleNode(prefix + "NrKontrahenta", manager).InnerText;
                if (child.SelectSingleNode(prefix + "NazwaKontrahenta", manager) != null)
                    dokument.Contractor.Nazwa = child.SelectSingleNode(prefix + "NazwaKontrahenta", manager).InnerText;
                if (child.SelectSingleNode(prefix + "AdresKontrahenta", manager) != null)
                    dokument.Contractor.Adres = child.SelectSingleNode(prefix + "AdresKontrahenta", manager).InnerText;

                if (child.SelectSingleNode(prefix + "NrDostawcy", manager) != null)
                    dokument.Contractor.Nip = child.SelectSingleNode(prefix + "NrDostawcy", manager).InnerText;
                if (child.SelectSingleNode(prefix + "NazwaDostawcy", manager) != null)
                    dokument.Contractor.Nazwa = child.SelectSingleNode(prefix + "NazwaDostawcy", manager).InnerText;
                if (child.SelectSingleNode(prefix + "AdresDostawcy", manager) != null)
                    dokument.Contractor.Adres = child.SelectSingleNode(prefix + "AdresDostawcy", manager).InnerText;


                if (child.SelectSingleNode(prefix + "DowodSprzedazy", manager) != null)
                    dokument.NumerDokumentu = child.SelectSingleNode(prefix + "DowodSprzedazy", manager).InnerText;

                if (child.SelectSingleNode(prefix + "DowodZakupu", manager) != null)
                    dokument.NumerDokumentu = child.SelectSingleNode(prefix + "DowodZakupu", manager).InnerText;

                if (child.SelectSingleNode(prefix + "DataWystawienia", manager) != null)
                    dokument.DataWystawienia = Convert.ToDateTime(child.SelectSingleNode(prefix + "DataWystawienia", manager).InnerText);

                if (child.SelectSingleNode(prefix + "DataZakupu", manager) != null)
                    dokument.DataWystawienia = Convert.ToDateTime(child.SelectSingleNode(prefix + "DataZakupu", manager).InnerText);

                if (child.SelectSingleNode(prefix + "DataSprzedazy", manager) != null)
                    dokument.DataSprzedazy = Convert.ToDateTime(child.SelectSingleNode(prefix + "DataSprzedazy", manager).InnerText);
                else
                    if (child.SelectSingleNode(prefix + "DataWystawienia", manager) != null)
                    dokument.DataSprzedazy = Convert.ToDateTime(child.SelectSingleNode(prefix + "DataWystawienia", manager).InnerText);

                if (child.SelectSingleNode(prefix + "DataWplywu", manager) != null)
                    dokument.DataWplywu = Convert.ToDateTime(child.SelectSingleNode(prefix + "DataWplywu", manager).InnerText);

                if (child.SelectSingleNode(prefix + "K_10", manager) != null)
                    dokument.Pozycje.NetZW = child.SelectSingleNode(prefix + "K_10", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_11", manager) != null)
                    dokument.Pozycje.K_11 = child.SelectSingleNode(prefix + "K_11", manager).InnerText;

                if (child.SelectSingleNode("K_12") != null)
                    dokument.Pozycje.K_12 = child.SelectSingleNode("K_12").InnerText;

                if (child.SelectSingleNode(prefix + "K_13", manager) != null)
                    dokument.Pozycje.Net0 = child.SelectSingleNode(prefix + "K_13", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_15", manager) != null)
                    dokument.Pozycje.Net5 = child.SelectSingleNode(prefix + "K_15", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_16", manager) != null)
                    dokument.Pozycje.Vat5 = child.SelectSingleNode(prefix + "K_16", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_17", manager) != null)
                    dokument.Pozycje.Net8 = child.SelectSingleNode(prefix + "K_17", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_18", manager) != null)
                    dokument.Pozycje.Vat8 = child.SelectSingleNode(prefix + "K_18", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_19", manager) != null)
                    dokument.Pozycje.Net23 = child.SelectSingleNode(prefix + "K_19", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_20", manager) != null)
                    dokument.Pozycje.Vat23 = child.SelectSingleNode(prefix + "K_20", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_21", manager) != null)
                    dokument.Pozycje.K_21 = child.SelectSingleNode(prefix + "K_21", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_22", manager) != null)
                    dokument.Pozycje.K_22 = child.SelectSingleNode(prefix + "K_22", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_23", manager) != null)
                    dokument.Pozycje.K_23 = child.SelectSingleNode(prefix + "K_23", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_24", manager) != null)
                    dokument.Pozycje.K_24 = child.SelectSingleNode(prefix + "K_24", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_27", manager) != null)
                    dokument.Pozycje.K_27 = child.SelectSingleNode(prefix + "K_27", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_28", manager) != null)
                    dokument.Pozycje.K_28 = child.SelectSingleNode(prefix + "K_28", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_29", manager) != null)
                    dokument.Pozycje.K_29 = child.SelectSingleNode(prefix + "K_29", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_30", manager) != null)
                    dokument.Pozycje.K_30 = child.SelectSingleNode(prefix + "K_30", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_31", manager) != null)
                    dokument.Pozycje.K_31 = child.SelectSingleNode(prefix + "K_31", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_32", manager) != null)
                    dokument.Pozycje.K_32 = child.SelectSingleNode(prefix + "K_32", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_33", manager) != null)
                    dokument.Pozycje.K_33 = child.SelectSingleNode(prefix + "K_33", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_34", manager) != null)
                    dokument.Pozycje.K_34 = child.SelectSingleNode(prefix + "K_34", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_35", manager) != null)
                    dokument.Pozycje.K_35 = child.SelectSingleNode(prefix + "K_35", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_36", manager) != null)
                    dokument.Pozycje.K_36 = child.SelectSingleNode(prefix + "K_36", manager).InnerText;
                /*
                if (child.SelectSingleNode(prefix + "K_43", manager) != null)
                    dokument.Pozycje.K_43 = child.SelectSingleNode(prefix + "K_43", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_44", manager) != null)
                    dokument.Pozycje.K_44 = child.SelectSingleNode(prefix + "K_44", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_45", manager) != null)
                    dokument.Pozycje.ZakupNet = child.SelectSingleNode(prefix + "K_45", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_46", manager) != null)
                    dokument.Pozycje.ZakupVat = child.SelectSingleNode(prefix + "K_46", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_49", manager) != null)
                    dokument.Pozycje.K_49 = child.SelectSingleNode(prefix + "K_49", manager).InnerText;

                if (child.SelectSingleNode(prefix + "K_50", manager) != null)
                    dokument.Pozycje.K_50 = child.SelectSingleNode(prefix + "K_50", manager).InnerText;
                */
                lista.Add(dokument);
            }

            return lista;
        }


        public static int ImportDocuments(List<Dokument> listaDokumentow, Session session, string rootName, Params Params)
        {

            int count = 0;
            foreach (Dokument doc in listaDokumentow)
            {
                using (ITransaction transaction = session.Logout(true))
                {
                    EwidencjaVatModule vm = EwidencjaVatModule.GetInstance(transaction);
                    CoreModule cm = CoreModule.GetInstance(transaction);
                    CRMModule crm = CRMModule.GetInstance(transaction);
                    KsiegaModule ksm = KsiegaModule.GetInstance(transaction);
                    KasaModule km = KasaModule.GetInstance(transaction);

                    VATEwidencja ewidencja = null;

                    string symbolPozycji = string.Empty;
                    string rodzajPodmiotu = string.Empty;

                    try
                    {
                        symbolPozycji = doc.Pozycje.Lista.First()[3];
                        rodzajPodmiotu = doc.Pozycje.Lista.First()[4];
                    }
                    catch
                    {
                        throw new Exception("Błąd danych pozycji: " + doc.NumerDokumentu);
                    }

                    if (doc.TypDefinicji == "Sprzedaz")
                    {
                        ewidencja = new SprzedazEwidencja();
                        cm.DokEwidencja.AddRow((Row)ewidencja);
                       // ewidencja.Definicja = GetDefinicja(cm, symbolPozycji);
                       // ewidencja.DataOperacji = (Soneta.Types.Date)doc.DataSprzedazy;
                    }

                    if (doc.TypDefinicji == "Zakup")
                    {
                        ewidencja = new ZakupEwidencja();
                        cm.DokEwidencja.AddRow((Row)ewidencja);
                       // ewidencja.Definicja = GetDefinicja(cm, symbolPozycji);
                       // ewidencja.DataWplywu = (Soneta.Types.Date)doc.DataWplywu;
                    }

                    if (doc.TypDefinicji == "Nabycia nalezny")
                    {
                        ewidencja = new FWUENabyciaNależnyEwidencja();
                        cm.DokEwidencja.AddRow((Row)ewidencja);
                        ewidencja.Definicja = GetDefinicja(cm, symbolPozycji);
                        ewidencja.DataWplywu = (Soneta.Types.Date)doc.DataSprzedazy;

                    }

                    if (doc.TypDefinicji == "SAD")
                    {
                        ewidencja = new SADEwidencja();
                        cm.DokEwidencja.AddRow((Row)ewidencja);
                        ewidencja.Definicja = GetDefinicja(cm, symbolPozycji);
                        ewidencja.DataWplywu = (Soneta.Types.Date)doc.DataWplywu;
                    }

                    if (doc.TypDefinicji == "RKV")
                    {
                        ZbiorczyEwidencja ewidencja2 = new RozliczeniaKasoweVATEwidencja();
                        cm.DokEwidencja.AddRow((Row)ewidencja2);
                        ewidencja2.Definicja = GetDefinicja(cm, symbolPozycji);
                        ewidencja2.DataWplywu = (Soneta.Types.Date)doc.DataWplywu;
                        ewidencja2.DataDokumentu = (Soneta.Types.Date)doc.DataWystawienia;
                        ewidencja2.NumerDokumentu = doc.NumerDokumentu;
                        ewidencja2.Opis = "Import z JPK";
                    }

                    if (doc.TypDefinicji != "RKV")
                    {
                        try
                        {
                            ewidencja.DataDokumentu = (Soneta.Types.Date)doc.DataWystawienia;
                        }
                        catch
                        {
                        }

                        ewidencja.NumerDokumentu = doc.NumerDokumentu;
                        ewidencja.Podmiot = GetKontrahent(doc.Contractor, crm, session);
                        ewidencja.Opis = "Import z JPK";
                        ewidencja.PodlegaVAT = true;
                        ewidencja.DataEwidencji = Params.DataEwidencji;

                        foreach (string[] poz in doc.Pozycje.Lista)
                        {
                            ElemEwidencjiVAT ewidencjaVat = null;

                            if (doc.TypDefinicji == "Sprzedaz" || doc.TypDefinicji == "Nabycia nalezny")
                                ewidencjaVat = new ElemEwidencjiVATSprzedaz((VATEwidencja)ewidencja);

                            if (doc.TypDefinicji == "Zakup" || doc.TypDefinicji == "SAD" || doc.TypDefinicji == "RKV")
                                ewidencjaVat = new ElemEwidencjiVATZakup((VATEwidencja)ewidencja);

                            vm.EleEwidencjiVATT.AddRow((Row)ewidencjaVat);
                            try
                            {
                                ewidencjaVat.DefinicjaStawki = cm.DefStawekVat.WgKodu[poz[0]];
                            }
                            catch
                            {
                                ewidencjaVat.DefinicjaStawki = cm.DefStawekVat.WgKodu["23%"];
                            }


                            ewidencjaVat.Netto = Convert.ToDecimal(poz[1].Replace(".", ","));
                            ewidencjaVat.VAT = Convert.ToDecimal(poz[2].Replace(".", ","));


                            if (doc.TypDefinicji == "Sprzedaz" || doc.TypDefinicji == "Nabycia nalezny")
                                ewidencjaVat.RodzajSprzedazy = (RodzajSprzedazyVAT)Enum.Parse(typeof(RodzajSprzedazyVAT), KonfiguracjaJPK.GetValue(cm.Session, symbolPozycji + "_R", "string"));

                            if (doc.TypDefinicji == "Zakup" || doc.TypDefinicji == "SAD" || doc.TypDefinicji == "RKV")
                                ewidencjaVat.Rodzaj = (RodzajZakupuVAT)Enum.Parse(typeof(RodzajZakupuVAT), KonfiguracjaJPK.GetValue(cm.Session, symbolPozycji + "_R", "string"));


                            ewidencjaVat.Naglowek.RodzajPodmiotu = GetRodzajPodmiotu(rodzajPodmiotu);

                            if (doc.TypDefinicji == "Nabycia nalezny")
                            {
                                if (symbolPozycji == "K_23" || symbolPozycji == "K_25")
                                    ewidencjaVat.Naglowek.DataPowstania = (Soneta.Types.Date)doc.DataWystawienia;
                                else
                                    ewidencjaVat.Naglowek.DataPowstania = (Soneta.Types.Date)doc.DataSprzedazy;
                            }

                            if (!Params.DataZaewidencjonowania.IsNull && (ewidencja.DataOperacji.Month != Params.DataEwidencji.Month || ewidencja.DataWplywu.Month != Params.DataEwidencji.Month))
                            {
                                ewidencjaVat.Naglowek.PowstanieObowiazku.Memorialowe = RozliczenieMemorialoweVAT.WgDowolnejDaty;
                                ewidencjaVat.Naglowek.DataPowstania = Params.DataZaewidencjonowania;
                            }

                        }
                    }


                    transaction.CommitUI();
                }

                count++;
            }

            return count;
        }


        public static Kontrahent GetKontrahent(Contractor contractor, CRMModule crm, Session session)
        {
            Kontrahent kontrahent = null;

            if (crm.Kontrahenci.WgNIP[contractor.Nip].GetNext() != null)
            {
                kontrahent = crm.Kontrahenci.WgNIP[contractor.Nip].GetNext();
            }
            /*else
            {
                using (ITransaction transaction = session.Logout(true))
                {
                    Kontrahent kontrahent2 = new Kontrahent();
                    crm.Kontrahenci.AddRow((Row)kontrahent2);
                    kontrahent2.Nazwa = contractor.Nazwa;

                    if (contractor.Nip.Length > 1)
                    {
                        if (contractor.Nip.Length < 14)
                            kontrahent2.EuVAT = contractor.Nip;
                        else
                            kontrahent2.EuVAT = contractor.Nip.Substring(0, 14);
                    }


                    if (contractor.Adres.Length < 66)
                        kontrahent2.Adres.Ulica = contractor.Adres;
                    else
                        kontrahent2.Adres.Ulica = contractor.Adres.Substring(0, 64);

                    kontrahent2.StatusPodmiotu = StatusPodmiotu.PodmiotGospodarczy;
                    kontrahent2.RodzajPodmiotu = RodzajPodmiotu.Krajowy;
                    kontrahent2.RodzajPodmiotuZakup = RodzajPodmiotu.Krajowy;
                    kontrahent = kontrahent2;
                    transaction.Commit();
                }

            }*/


            return kontrahent;
        }


        public static DefinicjaDokumentu GetDefinicja(CoreModule cm, string sekcja)
        {
            return cm.DefDokumentow.WgSymbolu[KonfiguracjaJPK.GetValue(cm.Session, sekcja, "string")];

        }

        public static RodzajPodmiotu GetRodzajPodmiotu(string rodzajPodmiotu)
        {
            switch (rodzajPodmiotu)
            {
                case "krajowy":
                    return RodzajPodmiotu.Krajowy;
                case "unijny":
                    return RodzajPodmiotu.Unijny;
                case "eksportowy":
                    return RodzajPodmiotu.Eksportowy;
                default:
                    return RodzajPodmiotu.Krajowy;
            }
        }


        public static string ProcentStawki(decimal netto, decimal vat)
        {
            int procent = (int)((vat * 100) / netto);
            return procent.ToString() + "%";
        }


    }
}
